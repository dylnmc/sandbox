
import System.IO (hFlush, stdout)

main = do
  putStr "Which sequence in the fibonacci do you desire? "
  hFlush stdout
  n <- readLn
  -- let n = (read input :: Integral)
  putStrLn $ show $ fibonacci n

fibonacci :: (Integral a) => a -> a
fibonacci 1 = 1
fibonacci 2 = 1
fibonacci n = fibonacci (n - 1) + fibonacci (n - 2)

