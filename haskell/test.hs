
-- main --
----------

-- main :: IO()
-- main = putStrLn "Hello from Haskell"
-- main = putStrLn $ greet "World"
-- greeting = "Hello"
-- greet who = greeting ++ ", " ++ who

-- functions --
---------------

-- add :: Int -> Int -> Int
add :: Num a => a -> a -> a
-- add a b = a + b
add = (+)

-- type synonyms --
-------------------

type Count = Int

-- data types --
--_-------------

data Compass = North | East | South | West
  deriving (Eq, Ord, Enum, Show)

-- use deriving instead of the following (for mechanical, simple things):

-- instance Show Compass where
--   show North = "North"
--   show East  = "East"
--   show South = "South"
--   show West  = "West"

-- instance Eq Compass where
--   North == North = True
--   East  == East  = True
--   South == South = True
--   West  == West  = True


data Expression = Number Int
                | Add Expression Expression
                | Subtract Expression Expression
                deriving (Eq, Ord, Show)

-- in ghci: Add (Number 1) (Number 2)
--        : Number 1 == Number 1
--        : Add (Number 1) (Subtract (Number 3) (Number 2))


-- Pattern Matching --
----------------------

calculate :: Expression -> Int
calculate (Number x) = x
calculate (Add x y) = (calculate x) + (calculate y)
calculate (Subtract x y) = (calculate x) - (calculate y)


-- Pattern Matching on Lists --
-------------------------------

newHead :: [a] -> a
newHead [] = error "empty list"
newHead (x:xs) = x

newTail :: [a] -> [a]
newTail [] = error "empty list"
newTail (x:xs) = xs


