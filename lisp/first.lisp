;;;; describe
;;; comment
;; indented comment
; post-code comment
#||
multiline comment
||#

(format t "Hello World ~%")
(print "What's your name")

(defvar *name* (read))

(defun hello-you (name)
  (format t "Hello ~a! ~%" name))

(setq *print-case* :capitalize)

(hello-you *name*)

(+ 5 4)

(defvar *number* 0)
(setf *number* 6)

(format t "Number with commas ~:d" 10000000)
(format t "PI to 5 characters ~5f ~%" 3.141593)
(format t "PI to 4 decimals ~,4f ~%" 3.141593)
(format t "10 percent ~,,2f ~%" .10)
(format t "10 Dollars ~$ ~%" 10)

(defvar *age* 18)
(if (= *age* 18)
  (progn
    (format t "Year you can vote~%")
    (format t "Congrats"))
  (format t "Unimportant year~%"))

(setf *age* 5)

(defun get-school (age)
  (case age
    (5 (print "Kindergarten"))
    (6 (print "First Grade"))
    (otherwise (print "middle school"))))

(get-school *age*)
