| #   | State            | Abr     | capital             |
| --- | ---------------- | :-----: | ------------------: |
| 1   | Alabama          | AL      | Montgomery          |
| 2   | Alaska           | AK      | Juneau              |
| 3   | Arizona          | AZ      | Phoenix             |
| 4   | Arkansas         | AR      | Little Rock         |
| 5   | California       | CA      | Sacramento          |
| 6   | Colorado         | CO      | Denver              |
| 7   | Connecticut      | CT      | Hartford            |
| 8   | Delaware         | DE      | Dover               |
| 9   | Florida          | FL      | Tallahassee         |
| 10  | Georgia          | GA      | Atlanta             |
| 11  | Hawaii           | HI      | Honolulu            |
| 12  | Idaho            | ID      | Boise               |
| 13  | Illinois         | IL      | Springfield         |
| 14  | Indiana          | IN      | Indianapolis        |
| 15  | Iowa             | IA      | Des Moines          |
| 16  | Kansas           | KS      | Topeka              |
| 17  | Kentucky         | KY      | Frankfort           |
| 18  | Louisiana        | LA      | Baton Rouge         |
| 19  | Maine            | ME      | Augusta             |
| 20  | Maryland         | MD      | Annapolis           |
| 21  | Massachusetts    | MA      | Boston              |
| 22  | Michigan         | MI      | Lansing             |
| 23  | Minnesota        | MN      | Saint Paul          |
| 24  | Mississippi      | MS      | Jackson             |
| 25  | Missouri         | MO      | Jefferson City      |
| 26  | Montana          | MT      | Helena              |
| 27  | Nebraska         | NE      | Lincoln             |
| 28  | Nevada           | NV      | Carson City         |
| 29  | New Hampshire    | NH      | Concord             |
| 30  | New Jersey       | NJ      | Trenton             |
| 31  | New Mexico       | NM      | Santa Fe            |
| 32  | New York         | NY      | Albany              |
| 33  | North Carolina   | NC      | Raleigh             |
| 34  | North Dakota     | ND      | Bismarck            |
| 35  | Ohio             | OH      | Columbus            |
| 36  | Oklahoma         | OK      | Oklahoma City       |
| 37  | Oregon           | OR      | Salem               |
| 38  | Pennsylvania     | PA      | Harrisburg          |
| 39  | Rhode Island     | RI      | Providence          |
| 40  | South Carolina   | SC      | Columbia            |
| 41  | South Dakota     | SD      | Pierre              |
| 42  | Tennessee        | TN      | Nashville           |
| 43  | Texas            | TX      | Austin              |
| 44  | Utah             | UT      | Salt Lake City      |
| 45  | Vermont          | VT      | Montpelier          |
| 46  | Virginia         | VA      | Richmond            |
| 47  | Washington       | WA      | Olympia             |
| 48  | West Virginia    | WV      | Charleston          |
| 49  | Wisconsin        | WI      | Madison             |
| 50  | Wyoming          | WY      | Cheyenne            |
