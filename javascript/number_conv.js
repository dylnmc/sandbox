
console.log(
	"| " + [1562, 4090, 100, 680, 15, 255].map(
		num => Array(4).fill(num).map(
			(n,i) => n.toString([10, 12, 2, 16, 8][i]).padEnd([7, 6, 12, 11, 6][i])
		).join(" | ")
	).join(" |\n| ") + " |"
);

