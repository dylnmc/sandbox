#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The Mad King
# ~~~~~~~~~~~~
#
# Two integers are input; when multiplied together they give the dimensions of a
# castle. Each can be positive, negative, or zero. If the products are zero or
# negative, then print "My Lord, I cry your mercy?"; otherwise, print the
# product
#
# Example Input:
# > 5 6
# Output:
# < 30
#
# Example Input:
# > 4 0
# Example Output:
# < My Lord, I cry your mercy?

a, b = input().split()
p = int(a) * int(b)
if p <= 0:
    print('My Lord, I cry your mercy?', end='')
else:
    print(p, end='')

# one-liner :D
# print(lambda x: 'My Lord, I cry your mercy?' if x <= 0 else x)((lambda a, b: int(a) * int(b))(input().split()), end='')
