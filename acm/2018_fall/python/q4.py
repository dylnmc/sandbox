#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from sys import stdout

for n in range(int(input())):
    r, i, s = input().split()
    r = int(r)
    i = int(i)
    l = s[i-r-1:i+r]
    stdout.write(
            s[i - 1]
            if l[:int(len(l)/2)].lower() == l[int((len(l)+1)/2):][::-1].lower()
            else '!'
            )

# one-liner :D
# print(''.join((lambda l: (n[2][n[1] - 1] if l[:int(len(l)/2)].lower() == l[int((len(l)+1)/2):][::-1].lower() else '!'))(n[2][n[1]-n[0]-1:n[1]+n[0]]) for n in ((lambda r, i, s: (int(r), int(i), s))(*input().split()) for m in range(int(input())))), end='')
