#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from argparse import ArgumentParser
from os.path import isdir, isfile


def main():
    parser = ArgumentParser(
        description='Small script to test ACM programming challenges'
    )
    parser.add_argument('number', nargs=1, type=int)
    parser.add_argument('--type', '-t', info='the type of code; python '
                        'should be inside of a python/ directory, for '
                        'instance, and c++ code should be in a cpp/ folder',
                        default='python'
                        )
    args = parser.parse_args()
    indir = 'input'
    if not isdir(indir):
        print('input/ is not a directory')
        exit(1)
    # if not


def test():
    if True:
        print('Always true')
    else:
        print('banana')


if __name__ == '__main__':
    main()
