#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
testing curses
"""

import curses


def main(stdscr):
    """ main """
    # allow curses to use default foreground/background (39/49)
    curses.use_default_colors()

    # Clear screen
    stdscr.clear()

    curses.init_pair(1, curses.COLOR_RED, -1)
    curses.init_pair(2, curses.COLOR_GREEN, -1)
    stdscr.addstr("ERROR: I like tacos, but I don't have any.\n",
                  curses.color_pair(1))
    stdscr.addstr("SUCCESS: I found some tacos.\n", curses.color_pair(2))

    stdscr.refresh()  # make sure screen is refreshed
    stdscr.getkey()  # wait for user to press key


if __name__ == '__main__':
    curses.wrapper(main)
