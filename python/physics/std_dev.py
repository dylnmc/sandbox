#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
simple standard deviation calculator
"""

from math import sqrt
from sys import argv


def main():
    """ main
    """
    nums = []
    for ind, arg in enumerate(argv[1:]):
        print(ind, arg)
        if arg == "--":
            nums = tuple(get_split_nums(",".join(argv[ind+1:])))
            break
    if not nums:
        nums = tuple(get_split_nums(input("Enter list of numbers: ")))

    print("std dev: {}".format(get_std_dev(nums)))


def get_std_dev(nums):
    """ get_std_dev
            calculate standard deviation
            @param {list} nums  list of ints/floats
        """
    mean = sum(nums) / len(nums)
    return sqrt(sum(((x - mean) ** 2 for x in nums)) / (len(nums) - 1))


def get_split_nums(nums):
    """ getSplitInput
            generator that returns each number separated
            by commas
            @param {str} nums  comma-delim. str of numbers to parse
    """
    for num in nums.split(","):
        clean_num = None
        try:
            clean_num = float(num)
            clean_num = int(num)
        except ValueError:
            pass
        if clean_num is None:
            continue
        yield clean_num


if __name__ == "__main__":
    main()
