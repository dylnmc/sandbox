#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from enum import Enum
from math import e, pi
from random import random
from re import compile as recompile
from sys import stderr

class Token():
    """ Token Class """

    l_parens = ('(', '[')
    r_parens = (')', ']')
    number = recompile(r'^-?(?!\.$)\d*\.?\d*$')
    constants = ('e', 'pi', 'tau', 'rand')
    functions = ('ln', 'log', 'sqrt', 'sin', 'cos', 'tan', 'asin', 'acos', 'atan')
    operators = ('^', '+', '-', '*', '/', )
    op_precs  = ( 3,   1,   1,   2,   2)

    def __init__(self, val):
        """ initialize a token """
        # token type (one of TokenType.*)
        self.typ = self.__get_type__(val)
        # associativity (either 'left' or 'right')
        self.associativity = 'left'
        # operator/function precedence
        self.precedence = -1

        if self.typ is TokenType.NUMBER:
            if val == 'e':
                self.val = e
            elif val == 'pi':
                self.val = pi
            elif val == 'tau':
                self.val = 2 * pi
            elif val == 'rand':
                self.val = random()
            else:
                if val[-1] == '.':
                    self.val = val[:-1]
                try:
                    self.val = int(val)
                except ValueError:
                    self.val = float(val)
        else:
            self.val = val

        if self.typ == TokenType.OPERATOR:
            if val == "^":
                # ^ (power) has right precedence
                self.associativity = 'right'
            # get appropriate operator precedence
            self.precedence = Token.op_precs[Token.operators.index(val)]
        # functions (I believe) have lowest precedence
        # so, leave precedence at -1


    def __get_type__(self, val):
        """ get TokenType for this Token
                based on:
                    - val: current token stored as string
        """
        if val in Token.l_parens:
            return TokenType.L_PAREN

        if val in Token.r_parens:
            return TokenType.R_PAREN

        if val in Token.operators:
            return TokenType.OPERATOR

        if self.__check_re__(Token.number.search(val)):
            return TokenType.NUMBER

        if val in Token.constants:
            return TokenType.NUMBER

        if val in Token.functions:
            return TokenType.FUNCTION

        if val == 'x':
            return TokenType.X

        return  TokenType.INVALID


    def __check_re__(self, regexp):
        """ helper function that determines if a regex result is true or false
                @private
                @param regexp: a regex result
                @return: bool
        """
        if regexp is None:
            return False
        if regexp.end() == 0:
            return False
        return True


    def __repr__(self):
        return str(self.val) + ' (type: ' + str(self.typ) + ')'



class TokenType(Enum):
    """ TokenType Class """
    # x
    X = 0
    # sin,cos,tan,asin,acos,atan,max,min,round,int
    FUNCTION = 1
    # +,-,*,/ (LHS), ^ (RHS)
    OPERATOR = 2
    # [-1-9]+ or (e,pi,tau)
    NUMBER = 3
    # (,[
    L_PAREN = 4
    # ),]
    R_PAREN = 5
    # INVALID
    INVALID = -1

