#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
                 _
  __ _  ___ __ _| | ___   _ __  _   _
 / _` |/ __/ _` | |/ __| | '_ \| | | |
| (_| | (_| (_| | | (__ _| |_) | |_| |
 \__, |\___\__,_|_|\___(_) .__/ \__, |
 |___/                   |_|    |___/

small graphing calculator in python using tkinter
"""

from math import log, sqrt, sin, cos, tan, asin, acos, atan
from random import random
from token import Token, TokenType
import tkinter as tk


def main():
    c = Calc()
    c.load_equation(input('y = '))
    c.eval_rpn()
    master = tk.Tk()
    c.graph(master)
    master.mainloop()


class Calc(tk.Frame):

    """
        Simple Graphing Calculator

            parses tokens and graphs
    """


    def __init__(self, **kwargs):
        """ initialize
                **kwargs:
                    width: width of graphing canvas (in pixels)
                    height: height of graphing canvas (in pixels)
                    xmin: minimum x-value for graph
                    xmax: maximum x-value for graph
                    ymin: minimum y-value for graph
                    ymax: maximum y-value for graph
                    master: tk.Tk() window (optional)
        """
        self.width  = kwargs.get('width', 800)
        self.height = kwargs.get('height', 800)
        self.xmin   = kwargs.get('xmin', -10)
        self.xmax   = kwargs.get('xmax', 10)
        self.ymin   = kwargs.get('ymin', -10)
        self.ymax   = kwargs.get('ymax', 10)
        self.master = kwargs.get('master', None)


    def __parse_tokens__(self, s):
        """ parse tokens
                @private
                @param s: raw string to tokenize
                @returns: list[str]
        """
        tokens = []
        token = ""
        single_tokens = (
            '(', ')', '[', ']',
            '+', '-', '*', '/', '^',
        )
        for ch in s:
            if ch in single_tokens or ch == ' ':
                if token:
                    tokens.append(Token(token))
                if ch != ' ':
                    token = Token(ch)
                    if token.val in ('+', '-'):
                        if not tokens or tokens[-1].typ not in (TokenType.NUMBER, TokenType.X):
                            token.typ = TokenType.FUNCTION
                    tokens.append(token)
                token = ''
            else:
                if ch:
                    token += ch
        if token:
            token = Token(token)
            if token.val in ('+', '-'):
                if not tokens or tokens[-1].typ not in (TokenType.NUMBER, TokenType.X):
                    token.typ = TokenType.FUNCTION
            tokens.append(token)

        return tokens


    def load_equation(self, s):
        """ parse 's' to self.rpn (Reverse Polish Notation)
                @param s: raw inputted string
        """
        # operator stack
        opStack = []
        # Reverse Polish Notation (RPN) stack---for use with self.eval_rpn()
        self.rpn = []

        # run through each token in parsed tokens
        for token in self.__parse_tokens__(s):
            if token.typ in (TokenType.NUMBER, TokenType.X):
                # if variable or number, push token to rpn stack
                self.rpn.append(token)
            elif token.typ is TokenType.FUNCTION:
                # if function, push token to operator stack
                opStack.append(token)
            elif token.typ is TokenType.OPERATOR:
                # if operator, ...
                while opStack and\
                    (opStack[-1].typ is TokenType.FUNCTION or\
                    opStack[-1].precedence > token.precedence or\
                    (opStack[-1].precedence == token.precedence and\
                     opStack[-1].associativity == 'left') and\
                    opStack[-1].typ is not TokenType.L_PAREN):
                    # while ((there is a function at the top of the operator stack)
		    # or (there is an operator at the top of the operator stack with greater precedence)
		    # or (the operator at the top of the operator stack has equal precedence and is left associative))
		    # and (the operator at the top of the operator stack is not a left bracket)
                    self.rpn.append(opStack.pop())
                # push token to operator stack
                opStack.append(token)
            elif token.typ is TokenType.L_PAREN:
                # if left paren, push to operator stack
                opStack.append(token)
            elif token.typ is TokenType.R_PAREN:
                # if right paren, ...
                while opStack and opStack[-1].typ is not TokenType.L_PAREN:
                    # while operator at the top of the operator stack is not a left bracket
                    self.rpn.append(opStack.pop())
                if not opStack:
                    # mismatched parens (user runtime error)
                    raise RuntimeError('Mismatched parentheses')
                opStack.pop()

        while opStack:
            # while there are tokens on the operator stack ...
            if opStack[-1].typ is TokenType.L_PAREN or\
                opStack[-1].typ is TokenType.R_PAREN:
                # mismatched parens (user runtime error)
                raise RuntimeError('Mismatched parentheses')
            # push operator to rpn stack
            self.rpn.append(opStack.pop())


    def eval_rpn(self):
        """ evaluate self.rpn to self.xpts and self.ypts
                NOTE: set self.width, self.height, self.xmin, self.xmax,
                      self.ymin, and self.ymax *before* this function is called,
                      as this function creates x and y points based on these
                      values
        """
        # x points to graph
        self.xpts = []
        # y points to graph
        self.ypts = []
        # change (delta) in y per pixel
        dx = (self.xmax - self.xmin) / self.width
        for i in range(self.width):
            # for every x pixel ...
            # calculate the x value (in terms of the graph not pixels)
            x = self.xmin + i * (self.xmax - self.xmin) / self.width
            # the list to which the result will be evaluated
            result = []
            for token in self.rpn:
                # for each token in the rpn stack ...
                if token.typ is TokenType.NUMBER:
                    # if number, push to result stack
                    result.append(token.val)
                if token.typ is TokenType.X:
                    # if 'x', push current kx' value to result stack
                    result.append(x)

                # handle all functions appropriately
                elif token.typ is TokenType.FUNCTION:
                    if token.val == 'ln':
                        result.append(log(result.pop()))
                    elif token.val == 'log':
                        result.append(log(result.pop()), 10)
                    elif token.val == 'sqrt':
                        result.append(sqrt(result.pop()))
                    elif token.val == 'sin':
                        result.append(sin(result.pop()))
                    elif token.val == 'cos':
                        result.append(cos(result.pop()))
                    elif token.val == 'tan':
                        result.append(tan(result.pop()))
                    elif token.val == 'asin':
                        result.append(asin(result.pop()))
                    elif token.val == 'acos':
                        result.append(acos(result.pop()))
                    elif token.val == 'atan':
                        result.append(atan(result.pop()))
                    elif token.val == '+':
                        pass
                    elif token.val == '-':
                        result.append(-result.pop())

                # handle all operators appropriately
                elif token.typ is TokenType.OPERATOR:
                    if token.val == '+':
                        result.append(result.pop() + result.pop())
                    elif token.val == '-':
                        result.append(-(result.pop() - result.pop()))
                    elif token.val == '*':
                        result.append(result.pop() * result.pop())
                    elif token.val == '/':
                        result.append(1/(result.pop() / result.pop()))
                    elif token.val == '^':
                        tmp = result.pop()
                        result.append(pow(result.pop(), tmp))

            # push x pixel value to self.xpts
            self.xpts.append(i)
            # push y pixel value to self.ypts (NOTE that it must be converted
            # from the y graph value to the y pixel value)
            self.ypts.append(-(self.ymin + result[0]) / (self.ymax - self.ymin) * self.height)
            i += 1


    def graph(self, *args):
        """ graph the results stored into self.xpts and self.ypts
                *args:
                    0: tk.Tk() window for drawing graph
                        > THIS MUST BE PASSED IF NOT CREATED IN __init__()
        """

        if not args:
            # if master not passed in *args
            if not self.master:
                # if master also was not created with a previous self.graph or in __init__()
                print('No window for graphing. Please pass tk.Tk() to Calc.graph()')
                return
        else:
            # master passed as args[0]
            self.master = args[0]

        # (re)set geometry to width*height
        self.master.geometry('{}x{}'.format(self.width, self.height))
        super().__init__(self.master)

        # (re)create the canvas to which we shall draw
        self.canvas = tk.Canvas(self.master, width=self.width, height=self.height)
        self.canvas.configure(bg='white')
        self.canvas.pack()

        # graph each xpt, ypt
        flag = True
        for xpt,ypt in zip(self.xpts, self.ypts):
            if flag:
                prevX = xpt
                prevY = ypt
                flag = False
                continue
            self.canvas.create_line(prevX, prevY, xpt, ypt)
            prevX = xpt
            prevY = ypt

        self.pack()



if __name__ == '__main__':
    main()
