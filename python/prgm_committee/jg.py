#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " "
" "   This script grabs a random joke from goodbadjokes.com    "
 "    and prints it to the screen.                            " "
" "                                                            "
 "    Provided free of charge. No license.                    " "
" "                                                            "
 "    Enjoy!                                                  " "
" " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " """

from random import randint
from urllib.error import URLError
from urllib.request import urlopen

from bs4 import BeautifulSoup


def main():
    """ main

    let's just make sure this is not an import
    """
    url = "https://www.goodbadjokes.com/"

    with urlopen(url) as response:
        try:
            html = response.read()
        except URLError as error:
            print("error parsing url {}:\n\t==>{}".format(url, error))

    soup = BeautifulSoup(html, "html.parser")

    joke_containers = soup.findAll("div", {"class": "joke-body-wrap"})

    jokes = []
    for joke_container in joke_containers:
        joke = []
        for line in joke_container.findChildren(
                "a", {"class": "permalink"})[0].contents:
            try:
                clean_line = line.contents[0]
            except (AttributeError, IndexError):
                continue
            clean_line = clean_line.strip(" \r\n")
            if not clean_line:
                continue
            joke.append(clean_line)
        if joke:
            jokes.append(joke)

    print("Random Joke: ", end="\n\t> ")
    print("\n\t> ".join(jokes[randint(0, len(jokes) - 1)]))


if __name__ == "__main__":
    main()
