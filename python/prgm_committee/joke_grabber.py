
"""
print a joke from a website
"""

from bs4 import BeautifulSoup
from bs4 import NavigableString
from random import randint
from urllib.error import URLError
from urllib.request import urlopen

url = "https://www.goodbadjokes.com/"


try:
    with urlopen(url) as request:
        html = request.read()
except URLError:
    print("Error trying to open {}".format(url))
    exit(1)

soup = BeautifulSoup(html, "html.parser")

joke_containers = soup.findAll("div", {"class": "joke-body-wrap"})

jokes = []
for joke_container in joke_containers:
    joke = []
    for line in joke_container.findChildren("a", {"class": "permalink"})[0].contents:
        if type(line) == NavigableString:
            clean_lines = (str(line).strip(" \n\r"),)
        else:
            clean_lines = tuple(line.stripped_strings)
        for clean_line in clean_lines:
            if clean_line:
                joke.append(clean_line.strip())
    jokes.append(joke)

print("\n\t> ".join(jokes[randint(0, len(jokes) - 1)]))