#! /usr/bin/env python2

"""
This is an interesting script, to say the least, using simple animation in Tkinter.
 
A few of green circles start positioned at the top-left of the Tkinter panel when it
opens. Then, on a white background, the flies disperse and move in all directions in a
randomized way. If they hit the edge of the screen, they jump backwards as if it
shocked them. The random, even chaotic, movement of the circles makes them appear to be
a lot of flies buzzing around the screen.
 
The circles are animated entirely randomly; they move left and right to a certain velocity
as well as up and down to a certain velocity. The circles have a class-wide velocity and
randomness is introduced through a random acceleration in a given range. As noted, the
velocities are restricted to a minimum and maximum that the flies cannot jump across your
screen in one cycle.
 
To run:
    * be sure that you have python-tk installed (search google for your operating system's
      version of this)
        - linux: "sudo apt-get install python-tk" in terminal
        - mac: should come with python
        - windows: ? (install from internet or maybe comes with python)
    * download this file (preferably to Downloads)
    * open terminal
        - ctrl+alt+t on most linux-based operating systems
        - cmd+Space + type: "terminal" on mac
        - windows-key + type "cmd" on windows (you will also need to install python in windows;
          see not below)
    * type "cd Downloads" (or wherever you saved this)
    * run the script
        - linux: "python flies.py"
        - mac: "/usr/bin/python2 flies.py"
        - windows: "\python\python.exe flies.py" (?)
    * enjoy
"""

__author__="dylnmc"

import random
from random import randint
from signal import signal, SIGINT
from sys import exit, stdout
from Tkinter import *

# EDIT this if you want MORE or LESS fies
numFlies = 100

def main():
    fb = FlyBox()
    fb.play()

class FlyBox():

    def __init__(self):
        # handle KeyboardInterrupt from terminal
        signal(SIGINT, self.signal_handler)
        
        # create Tk panel
        self.root = Tk()
        self.root.wm_title("Flies")
        try:
            self.root.attributes("-zoomed", True)
        except:
            self.root.attributes("-fullscreen", True)
        self.root.attributes("-alpha", .8)
        
        width = self.root.winfo_screenwidth()
        height = self.root.winfo_screenheight()
        
        # create drawing canvas
        self.canv = Canvas(self.root, width=width, height=height)
        
        # create array of flies
        self.flies = []
        for i in range(numFlies):
            self.flies.append(fly(self.canv, randint(5, width - 5), randint(5, height - 5)))
        
        self.canv.pack()

    def play(self):
        self.root.after(10, self.run)
        self.root.mainloop()
    
    def run(self):
        for fly in self.flies:
            fly.next()
        
        self.root.update_idletasks()
        self.root.after(10, self.run)

    def signal_handler(self, signal, frame):
        stdout.write("\x1B[2D") # moves cursor back 2 so that when user enter ^c to quit, these characters don't appear
        self.root.quit() # close tkinter frame/panel

        
class fly():

    def __init__(self, canvas, x, y, **kwargs):
        self.canv = canvas
        self.x = x
        self.y = y
        self.vx = kwargs.get("x_velocity", random.uniform(-2, 2))
        self.vy = kwargs.get("y_velcity", random.uniform(-2, 2))
        self.ax = None
        self.ay = None

        self.maxV = 3

        self.len = kwargs.get("radius", 4)

        self.circ = self.canv.create_oval(self.x, self.y, self.x + self.len, self.y + self.len, fill="green", outline="grey80")

    def nextAccelerations(self):
        self.ax = random.uniform(-.5, .5)
        self.ay = random.uniform(-.5, .5)

    def nextVelocities(self):
        a = self.nextAccelerations()

        self.vx = max(-self.maxV, min(self.maxV, self.vx + self.ax))
        self.vy = max(-self.maxV, min(self.maxV, self.vy + self.ay))

    
    def nextPositions(self):
        width = self.canv.winfo_width() - 15
        height = self.canv.winfo_height() - 2
        
        self.nextVelocities()
        
        self.x += self.vx
        self.y += self.vy
        
        if self.x + self.vx < 0:
            self.x = 0
            self.vx *= -1.5
        if self.x + self.len + self.vx > width:
            self.x = width - self.len
            self.vx *= -1.5
        if self.y + self.vy < 0:
            self.y = 0
            self.vy *= -2
        if self.y + self.len + self.vy > height:
            self.y = height - self.len
            self.vy *= -2

        
        

    def next(self):
        v = self.nextPositions()
        self.canv.coords(self.circ, self.x, self.y, self.x + self.len, self.y + self.len)


if __name__ == "__main__":
    main()

