#!/usr/bin/env python3

r"""     _
 ___(_)_ __   _____      ____ ___   _____   _ __  _   _
/ __| | '_ \ / _ \ \ /\ / / _` \ \ / / _ \ | '_ \| | | |
\__ \ | | | |  __/\ V  V / (_| |\ V /  __/_| |_) | |_| |
|___/_|_| |_|\___| \_/\_/ \__,_| \_/ \___(_) .__/ \__, |
small fun ascii sine wave generator        |_|    |___/
"""

from argparse import ArgumentParser, RawDescriptionHelpFormatter
from math import pi, sin
from sys import stderr

def main():
    parser = ArgumentParser(
        formatter_class = RawDescriptionHelpFormatter,
        description = __doc__
    )
    parser.add_argument('-A', '--amplitude', type=float, metavar='{amp}', help='Amplitude of the wave')
    parser.add_argument('-l', '--wavelen', type=float, metavar='{wlen}', help='Wavelength of one cycle')
    parser.add_argument('-L', '--length', type=float, metavar='{len}', help='Length of the wave')

    args = parser.parse_args()

    while args.amplitude is None:
        try: args.amplitude = float(input("Amplitude of wave: "))
        except ValueError: warn("Amplitude must be a valid number. Try again.")
    while args.wavelen is None:
        try: args.wavelen = float(input("Wavelength of wave: "))
        except ValueError: warn("Wavelength must be a valid number. Try again.")
    while args.length is None:
        try: args.length = float(input("Length of wave: "))
        except ValueError: warn("Length must be a valid number. Try again.")

    args.amplitude = round(args.amplitude)
    args.wavelen = round(args.wavelen)
    args.length = round(args.length)

    wave = []
    for y in range(args.amplitude * 2 + 1):
        wave.append([" "]  * args.length)

    for x in range(args.length):
        y = args.amplitude * sin( 2 * pi / args.wavelen * x) + args.amplitude
        wave[round(y)][x] = "*"

    for line in wave:
        print("".join(line))


def warn(msg):
    stderr.write("*WARNING* {}\n".format(msg))
    stderr.flush();


if __name__ == '__main__':
    main()
