#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from sys import argv, stderr

# globals
# max_long = 4
max_long = 8
# max_long = 16


def main():
    global max_long
    n = tuple(
            ord(i)-48 for i in
            (argv[1] if len(argv) > 1 else input('number: '))
            if i in '0123456789')
    if not n:
        stderr.write("Invalid number")
        exit(1)
    print(n)


if __name__ == '__main__':
    main()
