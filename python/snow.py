#! /usr/bin/env python2

"""
This is one of the cooler python scripts that uses simple animation in Tkinter.

A bunch of white circles start positioned at the top of the Tkinter panel when it opens.
Then, on a light grey background, the snowflakes gently fall downwards. When they reach
the bottom, they apparently disappear and start back above the top of the screen and
fall back into view. This continuous and soothing snow fall was so relaxing that I made
it open in the background of my mac terminal, which had a transparent background.

The circles are animated entirely randomly; they move left and right to a certain velocity
and can move both up and down, although the allowed downward velocity - naturally - is
faster than that of the upward velocity, which is very minimal. The circles have a class-
wide velocity and randomness is introduced through a random acceleration in a given range.
As noted, the velocities are restricted to a minimum and maximum to direct the circles
so as to give the appearance of snow.

To run:
    * be sure that you have python-tk installed (search google for your operating system's
      version of this)
        - linux: "sudo apt-get install python-tk" in terminal
        - mac: should come with python
        - windows: ? (install from internet or maybe comes with python)
    * download this file (preferably to Downloads)
    * open terminal
        - ctrl+alt+t on most linux-based operating systems
        - cmd+Space + type: "terminal" on mac
        - windows-key + type "cmd" on windows (you will also need to install python in windows;
          see not below)
    * type "cd Downloads" (or wherever you saved this)
    * run the script
        - linux: "python snow.py"
        - mac: "/usr/bin/python2 snow.py"
        - windows: "\python\python.exe snow.py" (?)
    * enjoy
"""

__author__="dylnmc"

from commands import getoutput as getoutp
from random import randint, uniform
from signal import signal, SIGINT
from sys import exit, stdout
from Tkinter import *

def main():
    sm = SnowMachine()
    sm.play()

class SnowMachine():

    def __init__(self):
        # handle ctrl+c from terminal
        signal(SIGINT, self.signal_handler)
        
        # create Tk panel
        self.root = Tk()
        self.root.wm_title("snowflakes")
        try:
            self.root.attributes("-zoomed", True)
        except:
            self.root.attributes("-fullscreen", True)
        
        width = self.root.winfo_screenwidth()
        height = self.root.winfo_screenheight()
        
        # create drawing canvas
        self.canv = Canvas(self.root, width=width, height=height)
        self.canv.pack()
        self.canv.configure(bg="grey65")
        
        # make snowflaks
        self.snowflakes = []
        for i in range(500):
            self.snowflakes.append(snowflake(self.canv, randint(0, width), randint(0, height / 16), length=uniform(4, 8)))

    def play(self):
        self.root.after(50, self.run)
        self.root.mainloop()
    
    def run(self):
        for snowflake in self.snowflakes:
            snowflake.next()
        self.root.update_idletasks()
        self.root.after(10, self.run)

    def signal_handler(self, signal, frame):
        stdout.write("\x1B[2D") # moves cursor back 2 so that when user enter ^c to quit, these characters don't appear
        self.root.quit() # close tkinter frame/panel

        
class snowflake():

    def __init__(self, canvas, x, y, **kwargs):
        self.canv = canvas
        self.x = x
        self.y = y
        self.vx = kwargs.get("x_velocity", uniform(1, 4))
        self.vy = kwargs.get("y_velcity", uniform(-4, 1))
        self.ax = None
        self.ay = None

        self.len = kwargs.get("length", 3)

        self.circ = self.canv.create_oval(self.x, self.y, self.x + self.len, self.y + self.len, outline="grey98", fill="grey98")

    def nextAccelerations(self):
        self.ax = uniform(-.5, .5)
        self.ay = uniform(-.5, .5)

    def nextVelocities(self):
        self.nextAccelerations()

        self.vx = max(-4, min(4, self.vx + self.ax))
        self.vy = max(-.5, min(6, self.vy + self.ay))

    
    def nextPositions(self):
        width = self.canv.winfo_width() - 15
        height = self.canv.winfo_height() + 3
        
        self.nextVelocities()
        
        self.x += self.vx
        self.y += self.vy
        
        if self.x + self.vx + self.len < 0:
            self.x = width - 2 + self.len
        if self.x + self.vx > width:
            self.x = 2 - self.len
        if self.y + self.vy - self.len > height:
            self.y = -self.len
            self.x = randint(0, width)
            self.vy = uniform(.5, 2)
        
        

    def next(self):
        v = self.nextPositions()
        self.canv.coords(self.circ, self.x, self.y, self.x + self.len, self.y + self.len)


if __name__ == "__main__":
    main()

