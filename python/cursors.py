#! /usr/bin/env python2
"""
View all of the cursors for your operating system and their names in a nice Tkinter-based Python script.
"""

__author__ = "dylnmc"

from Tkinter import *

# All of the cursors I could find - ordered very nicely for you :)
cursors = (
    ("arrows",                    ("arrow", "left_ptr", "top_left_arrow", "circle", "right_ptr", "draft_large", "draft_small", "center_ptr", )),
    ("mouse buttons",             ("leftbutton", "middlebutton", "rightbutton", "mouse", )),
    ("useful mouse buttons",      ("watch", "fleur", "hand1", "hand2", "xterm", "question_arrow", "exchange", "plus", "X_cursor","pirate", "heart", )),
    ("crosses",                   ("tcross", "cross", "cross_reverse", "diamond_cross", )),
    ("targets",                   ("crosshair", "dotbox",  "draped_box","target", "icon", )),
    ("bases (v)",                 ("based_arrow_down", "based_arrow_up", )),
    ("sizing",                    ("sizing", )),
    ("double arrow",              ( "sb_h_double_arrow", "double_arrow", "sb_v_double_arrow", )),
    ("arrows",                    ("sb_left_arrow", "sb_right_arrow", "sb_up_arrow", "sb_down_arrow", )),
    ("resizers",                  ("left_side", "right_side", "top_side", "bottom_side", )),
    ("tees",                      ("left_tee", "right_tee", "top_tee", "bottom_tee", )),
    ("corners",                   ("top_left_corner", "top_right_corner", "bottom_left_corner", "bottom_right_corner", )),
    ("angles",                    ("ul_angle", "ur_angle", "ll_angle", "lr_angle", )),
    ("drawing",                   ("pencil", "spraycan", )),
    ("transporation",             ("boat", "sailboat", "shuttle", )),
    ("symbols",                   ("bogosity", "box_spiral", "rtl_logo", )),
    ("recognizable objects",      ("man", "umbrella", "gobbler", "spider", "star", "clock", )),
    ("semi-recognizable objects", ("trek",  "coffee_mug", "gumby", )),
    ("random",                    ("dot", "iron_cross", )),
)
 
root = None # tkinter root panel
curCatInd = None # cursor category index
curNameInd = None # cursor name index
curCat = None # cursor category
curName = None # cursor name
 
def main():
    global root, cursors, curCatInd, curNameInd, curCat, curName
    # create tkinter panel
    root = Tk()
    for i in range(3):
        root.grid_columnconfigure(i, weight=1, uniform="normalColumn")
    for i in range(3):
        root.grid_rowconfigure(i, weight=1, uniform="normalRow")
    root.wm_attributes("-topmost", 1) # set it to be always on top
    root.minsize(816, 430)
    #root.maxsize(845, 690)
    Button(root, text = "<BACKWARD", command = backward).grid(column=0, row=2, sticky = W)
    Button(root, text = "FORWARD>", command = forward).grid(column=2, row=2, stick = W)
     
    # set curCatInd and curNameInd to first (0th) index
    curCatInd = 0
    curNameInd = 0
     
    # set up labels for pre-button clicks
    curCat = StringVar()
    curCat.set("category: {0}".format(cursors[curCatInd][0]))
    Label(root, textvariable = curCat).grid(column=0, row=0)
    curName = StringVar()
    curName.set("name: {0}".format(cursors[curCatInd][1][curNameInd]))
    Label(root, textvariable = curName).grid(column=0, row=1)
     
    # set up cursor for pre-button clicks
    root.config(cursor = cursors[curCatInd][1][curNameInd])
    root.mainloop()
 
def forward():
    global root, cursors, curCatInd, curNameInd, curCat, curName
    curNameInd += 1
    if curNameInd >= len(cursors[curCatInd][1]):
        curCatInd += 1
        if curCatInd >= len(cursors):
            curCatInd = 0
        curNameInd  = 0
    curCat.set("category: {0}".format(cursors[curCatInd][0]))
    curName.set("name: {0}".format(cursors[curCatInd][1][curNameInd]))
    root.update_idletasks()
    root.config(cursor = cursors[curCatInd][1][curNameInd])
 
def backward():
    global root, cursors, curCatInd, curNameInd, curCat, curName
    curNameInd -= 1
    if curNameInd <= -1:
        curCatInd -= 1
        if curCatInd <= -1:
            curCatInd = len(cursors) - 1
        curNameInd  = len(cursors[curCatInd][1]) - 1
    curCat.set("category: {0}".format(cursors[curCatInd][0]))
    curName.set("name: {0}".format(cursors[curCatInd][1][curNameInd]))
    root.update_idletasks()
    root.config(cursor = cursors[curCatInd][1][curNameInd])
 
if __name__ == "__main__":
    main()
