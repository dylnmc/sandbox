#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from os import system
from random import randint
from time import sleep


def main():
    while True:
        system('xdotool click 1')
        sleep(randint(10, 30))


if __name__ == '__main__':
    main()
