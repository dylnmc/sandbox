#! /usr/bin/env python2
"""
This is a very bubbly yet simple animation in python's Tkinter.

A lot of turquoise circles start positioned at the bottom of the Tkinter panel when
it opens. Then, on a blue background, The bubbles float fall upwards. Depending on their
size, the rise at different speeds: the bigger the faster. When they reach the top, they
apparently disappear and start back below the bottom of the screen and float back into
view. It's really fun to watch it for a minute or so.

The circles are animated entirely randomly; they move left and right to a certain velocity
and can move only upwards. The upward velocity depends on the size of the bubble, its x-
velocity and a randomly generated acceleration. Similarly, the x-acceleration is generated
and then added to a class-wide x-velocity that, like the y-velocity, has a minimum and
maximum. As mentioned, the velocities are restricted to a minimum and maximum to direct
the circles so as to give the appearance of bubbles rising through water.

To run:
    * be sure that you have python-tk installed (search google for your operating system's
      version of this)
        - linux: "sudo apt-get install python-tk" in terminal
        - mac: should come with python
        - windows: ? (install from internet or maybe comes with python)
    * download this file (preferably to Downloads)
    * open terminal
        - ctrl+alt+t on most linux-based operating systems
        - cmd+Space + type: "terminal" on mac
        - windows-key + type "cmd" on windows (you will also need to install python in windows;
          see not below)
    * type "cd Downloads" (or wherever you saved this)
    * run the script
        - linux: "python bubbles.py"
        - mac: "/usr/bin/python2 bubbles.py"
        - windows: "\python\python.exe bubbles.py" (?)
    * enjoy
"""

__author__="dylnmc"

from commands import getoutput as getoutp
from math import log
from random import randint, uniform
from signal import signal, SIGINT
from sys import exit, stdout
from Tkinter import *

def main():
    bm = BubbleMachine()
    bm.play()

class BubbleMachine():

    def __init__(self):
        # handle ctrl+c from terminal
        signal(SIGINT, self.signal_handler)

        # create Tk panel
        self.root = Tk()
        self.root.wm_title("bubbles! bubbles! bubbles!")
        try:
            self.root.attributes("-zoomed", True)
        except:
            self.root.attributes("-fullscreen", True)

        width = self.root.winfo_screenwidth()
        height = self.root.winfo_screenheight()

        # create drawing canvas
        self.canv = Canvas(self.root, width=width, height=height)
        self.canv.pack()
        self.canv.configure(bg="#002b36")

        # make bubble
        self.bubbles = []
        for i in range(500):
            self.bubbles.append(bubble(self.canv, randint(0, width), randint(0, height), length=uniform(6, 44)))

    def play(self):
        self.root.after(50, self.run)
        self.root.mainloop()

    def run(self):
        for bubble in self.bubbles:
            bubble.next()
        self.root.update_idletasks()
        self.root.after(15, self.run)

    def signal_handler(self, signal, frame):
        stdout.write("\x1B[2D") # moves cursor back 2 so that when user enter ^c to quit, these characters don't appear
        self.root.quit() # close tkinter frame/panel


class bubble():

    def __init__(self, canvas, x, y, **kwargs):
        self.canv = canvas
        self.x = x
        self.y = y
        self.vx = kwargs.get("x_velocity", uniform(1, 4))
        self.vy = kwargs.get("y_velcity", uniform(-4, 1))
        self.ax = None
        self.ay = None

        self.len = kwargs.get("length", 3)

        self.circ = self.canv.create_oval(self.x, self.y, self.x + self.len, self.y + self.len, outline="#2aa198", fill="#2aa198")

    def nextAccelerations(self):
        self.ax = uniform(-.4, .4)
        self.ay = uniform(-.5, .5)

    def nextVelocities(self):
        self.nextAccelerations()

        self.vx = max(-1, min(1, self.vx + self.ax))
        self.vy = max(-2 * log(self.len) * .75 + self.vx, min(-1, self.vy + self.ay))


    def nextPositions(self):
        width = self.canv.winfo_width() - 15
        height = self.canv.winfo_height() + 3

        self.nextVelocities()

        self.x += self.vx
        self.y += self.vy

        if self.x + self.vx + self.len < 0 and self.vx < 0:
            self.x = width + self.len
        if self.x + self.vx > width and self.vx > 0:
            self.x = -self.len
        if self.y + self.vy < 0:
            self.y = height
            self.x = randint(0, width)



    def next(self):
        v = self.nextPositions()
        self.canv.coords(self.circ, self.x, self.y, self.x + self.len, self.y + self.len)


if __name__ == "__main__":
    main()

