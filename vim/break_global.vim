
function! TestMatch1()
	echom "Change me to single quotes"
endfunction

function! TestIgnore1()
	echom "\<lt>Leave me as double quotes> "
endfunction

function! TestMatch2()
	echom "Change me to single quotes"
endfunction

function! TestMatch3()
	echom "Change me to single quotes"
endfunction

function! TestMatch4()
	echom "Change me to single quotes"
endfunction

function! TestIgnore2()
	echom "\<lt>Leave me as double quotes> "
endfunction

function! TestMatch5()
	echom "Change me to single quotes"
endfunction
