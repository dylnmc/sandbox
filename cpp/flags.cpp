#include <iostream>
#include <string>

using namespace std;

int main()
{
    ios_base::fmtflags flags = cout.flags();
    string sflags;

    if( flags & ios_base::skipws ) sflags += "skipws";
    if( flags & ios_base::unitbuf ) sflags += sflags.empty() ? "unitbuf" : " unitbuf";
    if( flags & ios_base::uppercase ) sflags += sflags.empty() ? "uppercase" : " uppercase";
    if( flags & ios_base::showbase ) sflags += sflags.empty() ? "showbase" : " showbase";
    if( flags & ios_base::showpoint ) sflags += sflags.empty() ? "showpoint" : " showpoint";
    if( flags & ios_base::showpos ) sflags += sflags.empty() ? "showpos" : " showpos";
    if( flags & ios_base::left ) sflags += sflags.empty() ? "left" : " left";
    if( flags & ios_base::right ) sflags += sflags.empty() ? "right" : " right";
    if( flags & ios_base::internal ) sflags += sflags.empty() ? "internal" : " internal";
    if( flags & ios_base::dec ) sflags += sflags.empty() ? "dec" : " dec";
    if( flags & ios_base::oct ) sflags += sflags.empty() ? "oct" : " oct";
    if( flags & ios_base::hex ) sflags += sflags.empty() ? "hex" : " hex";
    if( flags & ios_base::scientific ) sflags += sflags.empty() ? "scientific" : " scientific";
    if( flags & ios_base::fixed ) sflags += sflags.empty() ? "fixed" : " fixed";
    /* if( flags & ios_base::hexfloat ) sflags += sflags.empty() ? "hexfloat" : " hexfloat"; */
    if( flags & ios_base::boolalpha ) sflags += sflags.empty() ? "boolalpha" : " boolalpha";
    /* if( flags & ios_base::_Stdio ) sflags += sflags.empty() ? "_Stdio" : " _Stdio"; */

    cout << "Standard flags from cout stream: " << sflags << endl;
}
