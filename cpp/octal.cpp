#include <iostream>
using namespace std;
int main(int argc, char* argv[]) {
	cout << "012: " << 012 << endl;
	cout << "give me a number: ";
	int x;
	cin >> x;
	cout << "\nthe number: " << x << endl;
	cout << "\nThere is an inconsistency with octal numbers hence the question.\n";
	cout << "Of course, it makes more sense to use base-10; I was just curious" << endl;
}
